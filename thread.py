import collections, time, json

from threading import Thread
from datetime import datetime

from tools.paralel_tools import process_files_from, add_to_collections_index
from tools.decorators import with_clock_and_json_export

DIRECTORIES = [f'.\\dataset\\{path}\\' for path in range(1,6)]

@with_clock_and_json_export
def main():
    index = collections.defaultdict(list)
    threads = []

    for i in DIRECTORIES:
        th = Thread(target=process_files_from,args=(i, index))
        threads.append(th)
        th.start()

    for th in threads:
        th.join()
    
    return index

if __name__ == "__main__":
    main()