import collections

from tools.get_files import get_files
from tools.tools import process_file, add_to_index
from tools.decorators import with_clock_and_json_export

@with_clock_and_json_export
def main():
    files = get_files()

    index = collections.defaultdict(list)

    for path in files:
        add_to_index(
            index,
            process_file(path),
            path
        )

    return index

if __name__ == "__main__":
    main()