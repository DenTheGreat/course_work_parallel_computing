import multiprocessing, collections

from tools.get_files import get_files
from tools.tools import process_file, add_to_index
from tools.decorators import with_clock_and_json_export

@with_clock_and_json_export
def main():
    files = get_files()

    index = collections.defaultdict(list)
    
    with multiprocessing.Pool(5) as pool:
        data = pool.map(process_file,files)

    for i, path in enumerate(files, start=0):
        add_to_index(
            index,
            data[i],
            path
        )

    return index

if __name__ == '__main__' :
    main()