import os

from .tools import process_file

def chunker(seq, number):
    size = int(len(seq)/number)
    return (seq[pos:pos + size] for pos in range(0, len(seq), size))

def process_chunk(chunk, index):
    for path in chunk:
        add_to_collections_index(
            index,
            process_file(path),
            path
        )

def process_chunk_default(chunk, index):
    for path in chunk:
        add_to_index(
            index,
            process_file(path),
            path
        )

def process_files_from(path, index):
    for root, directory, file in os.walk(path):
        for f in file:
            if '.txt' in f:
                path = os.path.join(root, f)
                words = process_file(path)
                add_to_collections_index(index, words, path)


def add_to_index(index, entry, key):
    for word in entry:
        if word in index:
            index[word].append(key)
        else:
            index[word] = [key]

def add_to_collections_index(index, entry, key):
    for word in entry:
        index[word].append(key)

    


