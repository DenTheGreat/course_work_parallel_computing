import os

PATH = '.\\dataset\\'

def get_files():
    files = []

    for root, directory, file in os.walk(PATH):
        for f in file:
            if '.txt' in f:
                files.append(os.path.join(root, f))
    
    return files