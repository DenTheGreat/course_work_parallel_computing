import time, json

from datetime import datetime

def with_clock_and_json_export(func):
    def wrapper():
        start_time = time.time()
        index = func()
        print("--- %s seconds ---" % (time.time() - start_time))

        print("Index length:", len(index))

        with open(f'''Index from {datetime.now().strftime("%d-%m-%Y_%H'%M'%S")}.json''', 'w') as outfile:
            json.dump(index, outfile)

    return wrapper

def with_clock_and_json_export_async(func):
    async def wrapper():
        start_time = time.time()
        index = await func()
        print("--- %s seconds ---" % (time.time() - start_time))

        print("Index length:", len(index))

        with open(f'''Index from {datetime.now().strftime("%d-%m-%Y_%H'%M'%S")}.json''', 'w') as outfile:
            json.dump(index, outfile)

    return wrapper
