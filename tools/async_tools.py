from .tools import REPLACABLES

async def clear_string(string):
    for pattern in REPLACABLES:
        string = string.replace(pattern,' ')
    return string

async def get_unique_words(arr):
    return set(arr)

async def process_file(path):
    file = open(path, "r", encoding='utf-8')
    data = await clear_string(file.read())
    data = data \
        .lower() \
        .split(' ')
    return await get_unique_words(data)

async def make_index(path):
    return dict.fromkeys(await process_file(path), path)
