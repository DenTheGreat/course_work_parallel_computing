REPLACABLES = ['<br />','"','(','!',".","?",',','*',')',':',';','/']

def clear_string(string):
    for pattern in REPLACABLES:
        string = string.replace(pattern,' ')
    return string

get_unique_words = lambda arr : list(set(arr))

def process_file(path):
    with open(path, "r", encoding='utf-8') as file:
        data = clear_string(file.read()) \
            .lower() \
            .split(' ')
        return get_unique_words(data)

def add_to_index(index, entry, key):
    for word in entry:
        index[word].append(key)
