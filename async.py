import collections
import asyncio

from tools.get_files import get_files
from tools.async_tools import process_file, make_index 
from tools.decorators import with_clock_and_json_export_async

@with_clock_and_json_export_async
async def main():

    files = get_files()
    
    indexes, _ = await asyncio.wait([ make_index(path) for path in files ])

    
    index = collections.defaultdict(list)

    for d in indexes:
        for key, value in d.result().items():
            index[key].append(value)

    return index

if __name__ == "__main__":
    loop = asyncio.get_event_loop()
    loop.run_until_complete(main())
    loop.close()