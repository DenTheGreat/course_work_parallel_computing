import collections
from threading import Thread, Lock

from tools.paralel_tools import process_chunk, chunker
from tools.get_files import get_files
from tools.decorators import with_clock_and_json_export

NUMBER_OF_THREADS = 8

@with_clock_and_json_export
def main():
    files = get_files()
    chunks = chunker(files,NUMBER_OF_THREADS)

    index = collections.defaultdict(list)
    threads = []

    for chunk in chunks:
        th = Thread(target=process_chunk,args=(chunk, index))
        threads.append(th)
        th.start()

    for th in threads:
        th.join()

    return index

if __name__ == "__main__":
    main()