# Построение инвертированного индекса

## Требования

- `python 3.7+`

- [Датасет](https://drive.google.com/file/d/1xL4eV2OirOoR7QiwGMSQ1zaxzt8_nEki/view?usp=sharing), расположенный в корневой папке

## Запуск

- `$ python consecutive.py`  - последовательный алгоритм

- `$ python async.py` - асинхронный алгоритм

- `$ python thread.py` - полностью многопоточный алгоритм

- `$ python thread_chunks.py` - частично многопоточный алгоритм

- `$ python worker_pool.py` - алгоритм с использованием пула рабочих процессов